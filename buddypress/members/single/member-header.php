<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>
<?php
/*-----------------------------------------------------------------------------------*/
/* Date: 09/16/12 */
/* Description:  REST webservice call to SalesForce.com to retreive campaign details 
/*-----------------------------------------------------------------------------------*/
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
	//Get the user data and format into the campaign name
	global $bp;
	$user_info = get_userdata( $bp->displayed_user->id ); 
	$user_email = $user_info->user_email;
	$campaign_id = bp_get_profile_field_data( 'field=Campaign ID' );
	$video = bp_get_profile_field_data('field=Video');
/*-----------------------------------------------------------------------------------*/
/* End REST webservice call
/*-----------------------------------------------------------------------------------*/
?>



<div>

	<div style="max-width: 200px;">
		<div id="item-header-avatar">
			<?php bp_displayed_user_avatar( 'type=full' ); ?>
			<?php //do_action('bp_member_online_status', bp_displayed_user_id()); ?>
		</div><!-- #item-header-avatar -->

		<?= gokleo_get_share_buttons(); ?>
	</div>

	<div id="right-column">
		<? if($video) { ?>
		<div class="video-container">
		    <? if (substr($video, 0, 7) == 'ginger:') { ?>
		      <? $ginger = substr($video, 7); ?>
		      <iframe src="http://www.ccv.adobe.com/v1/player/<?=$ginger;?>/embed" frameborder="0" allowfullscreen></iframe>
		    <? } else if (substr($video, 0, 6) == 'vimeo:') { ?>
		      <? $vimeo = substr($video, 6); ?>
		      <iframe src="//player.vimeo.com/video/<?=$vimeo?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		    <? } else { ?>
			  <iframe class="video" src="http://www.youtube.com/embed/<?= $video?>?rel=0&autoplay=0&showinfo=0&controls=2" frameborder="0" allowfullscreen></iframe>
		    <? } ?>
		</div>
		<? } ?>
		
		<div id='item-inspiration'>
			<?= xprofile_get_field_data( 'Inspiration', bp_get_member_user_id() ); ?>
		</div>


		<div style='padding-top: 25px;'>
			<?php if ( $data = bp_get_profile_field_data( 'field=Idea' ) ) : ?>
				<span class="champion-project">Fundraising for: <a href="<?php bp_profile_field_data( 'field=Idea URL' );?>"><?php bp_profile_field_data( 'field=Idea' );?></a></span>
				<?
				$contact = go_salesforce_get_contact( $user_email ); 

				// Display the most recent campaign
				if ($contact['campaigns'][0]) {
					echo go_salesforce_progress_bar($contact['campaigns'][0]['campaignId'], 'goal,amountRaised,daysLeft,donateButton');				
				}

				?>
			<?php else : ?>
				<span class="champion-project">Not currently fundraising.</span>
			<?php endif ?>
		</div>
	</div>

</div>


<div>


</div>

<div style='clear: both;'/>

<div id="item-header-content" <?php if (isset($_COOKIE['bp-profile-header']) && $_COOKIE['bp-profile-header'] == 'small') {echo 'style="display:none;"';} ?>>

	<?php if ( bp_is_active( 'activity' ) && bp_activity_do_mentions() ) : ?>
		<h4 class="user-nicename">@<?php bp_displayed_user_mentionname(); ?></h4>
	<?php endif; ?>

	<?php do_action( 'bp_before_member_header_meta' ); ?>

	<div id="item-meta">

		<div id="item-buttons">

			<?php do_action( 'bp_member_header_actions' ); ?>

		</div><!-- #item-buttons -->

		<?php
		/***
		 * If you'd like to show specific profile fields here use:
		 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
		 */
		 do_action( 'bp_profile_header_meta' );

		 ?>

	</div><!-- #item-meta -->

</div><!-- #item-header-content -->

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>