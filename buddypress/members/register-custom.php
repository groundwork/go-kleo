

<div class="register-section" id="profile-details-section" style="margin-top: 50px;">


	<?php /* Use the profile field loop to render input fields for the 'base' profile field group */ ?>
	<?php if ( bp_is_active( 'xprofile' ) ) : if ( bp_has_profile( array( 'profile_group_id' => go_get_profile_group_id(), 'fetch_field_data' => false ) ) ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

	<h4><?php _e( bp_the_profile_group_name().' Details', 'buddypress' ); ?></h4>

	<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

        <div<?php bp_field_css_class( 'editfield' ); ?>>

			<?php
			$field_type = bp_xprofile_create_field_type( bp_get_the_profile_field_type() );
			$field_type->edit_field_html();

			if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
				<p class="field-visibility-settings-toggle" id="field-visibility-settings-toggle-<?php bp_the_profile_field_id() ?>">
					<?php printf( __( 'This field can be seen by: <span class="current-visibility-level">%s</span>', 'buddypress' ), bp_get_the_profile_field_visibility_level_label() ) ?> <a href="#" class="visibility-toggle-link"><?php _ex( 'Change', 'Change profile field visibility level', 'buddypress' ); ?></a>
				</p>

				<div class="field-visibility-settings" id="field-visibility-settings-<?php bp_the_profile_field_id() ?>">
					<fieldset>
						<legend><?php _e( 'Who can see this field?', 'buddypress' ) ?></legend>

						<?php bp_profile_visibility_radio_buttons() ?>

					</fieldset>
					<a class="field-visibility-settings-close" href="#"><?php _e( 'Close', 'buddypress' ) ?></a>

				</div>
			<?php else : ?>
				<p class="field-visibility-settings-notoggle" id="field-visibility-settings-toggle-<?php bp_the_profile_field_id() ?>">
					<?php printf( __( 'This field can be seen by: <span class="current-visibility-level">%s</span>', 'buddypress' ), bp_get_the_profile_field_visibility_level_label() ) ?>
				</p>
			<?php endif ?>

			<!--<p class="description"><?php bp_the_profile_field_description(); ?></p>-->

		</div>

		<? if($_POST['champion-ideaId']) { ?>
			<? if(bp_get_the_profile_field_name() == "Idea ID") { ?>
				<script type="text/javascript">
					field = document.getElementById('field_<?= bp_get_the_profile_field_id()?>')
					field.value = '<?= $_POST['champion-ideaId']?>';
					field.readOnly = true;
				</script>
			<? } ?>
			<? if(bp_get_the_profile_field_name() == "Idea URL") { ?>
				<script type="text/javascript">
					field = document.getElementById('field_<?= bp_get_the_profile_field_id()?>')
					field.value = '<?= $_POST['champion-url']?>';
					field.readOnly = true;
				</script>
			<? } ?>
			<? if(bp_get_the_profile_field_name() == "Idea") { ?>
				<script type="text/javascript">
					field = document.getElementById('field_<?= bp_get_the_profile_field_id()?>')
					field.value = '<?= $_POST['champion-idea']?>';
					field.readOnly = true;
				</script>
			<? } ?>
		<? } ?>

	<?php endwhile; ?>

	<?php endwhile; ?>

	<?php endif ?>

	<?php endif ?>



