<?php
/**
 * Template Name: GO Landing
 *
 * Description: GO Landing Page Template
 *
 * @package WordPress
 * @subpackage GO-Kleo
 */

get_header(); ?>

<style>

div.wrap-content {
	margin-top: -40px; /* hack to move the page up */
}

div.kleo-main-header {
	box-shadow: 2px 2px 2px #666;
}

.goland-section {
	width: 100%;
	height: 100vh;
	padding-top: 10px;
	background-color: #ccc;
	background-image: url('/wp-content/themes/go-kleo/assets/img/landing.jpg');
	background-size: cover;
	background-attachment: fixed;
	background-position: center center;
	z-index: 10;
}

.goland-box {
	width: 80%;
	margin-top: 10px;
	/*background-color: rgba(255, 255, 255, 0.8);*/
	padding: 10px;
	margin-left: auto;
	margin-right: auto;
	color: #000;
	/*text-shadow: 1px 1px 1px #ccc;*/
	/*box-shadow: 2px 4px 4px #ccc;*/
	border-radius: 5px;
}

.goland-infobox {
	width: 350px;
	margin-top: 10px;
	background-color: rgba(255, 255, 255, 0.8);
	left: 200px;
	position: absolute;
	padding: 10px;
	color: #000;
	text-shadow: 1px 1px 1px #ccc;
	box-shadow: 2px 4px 4px #ccc;
	border-radius: 5px;
}

.title {
	font-size: 2.3em;
	line-height: 1.2em;
	/*margin-bottom: 10px;*/
	color: rgba(255,255,255,1);
	text-shadow: 1px 1px 1px rgba(000,000,000,1), 2px 2px 4px rgba(106,106,106,0.8), -2px -2px 4px rgba(106,106,106,0.8);	
}

.info-title {
	font-size: 1.5em;
}

.news {
	font-size: 2.3em;
}

.watch-box {
	border-radius: 0px;
	background-color: rgba(0, 0, 0, 1);
	color: #fff;
	box-shadow: 2px 2px 2px #ccc;
	transition: 1s ease-in-out;
	height: 60vh;
	width: 100%;
	background-image: url('/wp-content/themes/go-kleo/assets/img/intro_video_still.png');
	background-size: contain;
	background-position: center center;
	background-repeat: no-repeat;
}

.scroll-box {
	background-color: rgba(255, 255, 255, 0.8);
	color: #000;
	text-shadow: 1px 1px 1px #ccc;
	width: 20%;
	left: 40vw;
	transition: 2s;
	min-width: 300px;
	margin: 0 auto;
}

.goland-infobox img {
	clear: both;
	float: left;
	margin: 5px;
}

.goland-infobox .content .highlight {
	color: #5BD6FF;
}

.goland-infobox .content {
	float: left;
	width: 150px;
	margin: 5px;
}


#infobox1 {
	left: 50%;
}
.stick1 {
	position: fixed;
	top: 5%;
	left: 50%;
}
 
 #infobox2 {
 	left: 5%;
 }
.stick2 {
	position: fixed;
	top: 20%;
	left: 10%;
}

#infobox3 {
	left: 55%;
	width: 400px;
}
.stick3 {
	position: fixed;
	top: 30%;
	left: 55%;
}

#infobox4 {
	left: 35%;
}
.stick4 {
	position: fixed;
	top: 55%;
	left: 35%;
}

#actionbox {
	margin-left: auto;
	margin-right: auto;
	width: 50%;
}
.actionbox {
	width: 40%;
	text-align: center;
	font-size: 1.8em;
	float: none;
	margin: 25px;
	background-color: rgba(255,255,255,1);
	border: 2px solid #aaa;
	box-shadow: 2px 2px 2px #ccc;
  margin-left: auto;
  margin-right: auto;
}
.stickab {
	position: fixed;
	top: 25%;
	left: 25%;
}

#main-container {
	padding: 0px;
	margin: 0px;
	max-width: 100%;
}

.center {
	text-align: center;
}

.video {
}

.video .img {
	margin-top: 30%;
}

.intro-video-container {
	display: none;
	position: absolute;
	top: 0;
	left: 0;
	background-color: rgba(0, 0, 0, 0.5);
}

.icon-play-round-circled::before {
	font-size: 6em;
}

.watch-box-play {
}

.watch-box-play #watch-title {
	display: none;
}

.watch-box-play #play-intro-video {
	display: none;
}

.watch-box-play .intro-video-container {
	display: block;
	transition-delay: 2s;
}



.ribbon {
 font-size: 0.6em;

 /*height: 2.1em;*/
 height: 100%;

 width: 100%;
    
 position: relative;
 background-color: #fff;
 color: #fff;
 text-align: center;

 box-shadow: 2px 2px 2px #666;

 padding-bottom: 4px;

 line-height: 1.8em;
}
.ribbon:before, .ribbon:after {
 content: "";
 position: absolute;
 display: block;
 bottom: -1.0em;
 border-left: 1.0em;
 border: 1.0em solid #fff;
 z-index: -1;
}
.ribbon:before {
 left: -1.5em;
 border-right-width: 1.5em;
 border-left-color: transparent;
 box-shadow: 2px 2px 2px #666;
 height: 100%;
}
.ribbon:after {
 right: -1.5em;
 border-left-width: 1.5em;
 border-right-color: transparent;
 box-shadow: -2px 2px 2px #666;
 height: 100%;
}
.ribbon .ribbon-content:before, .ribbon .ribbon-content:after {
 content: "";
 position: absolute;
 display: block;
 border-style: solid;
 border-color: #666 transparent transparent transparent;
 bottom: -1em;

}
.ribbon .ribbon-content:before {
 left: 0;
 border-width: 1em 0 0 1em; 
}
.ribbon .ribbon-content:after {
 right: 0;
 border-width: 1em 1em 0 0;
}

.ribbon .ribbon-content {
	text-shadow: none;
}

/* Smartphones (portrait and landscape) ----------- */
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 480px) {
/* Styles */

.stick1 {
	position: fixed;
	top: 10%;
	left: 10px;
}
.no-mobile {
	display: none;
}
</style>



<?php
//create full width template
kleo_switch_layout('no');
?>

<?php get_template_part('page-parts/general-before-wrap'); ?>


<?php
/* hide the post data for now */
function landing_post() {
if ( have_posts() ) :
	// Start the Loop.
	while ( have_posts() ) : the_post();

		/*
		 * Include the post format-specific template for the content. If you want to
		 * use this in a child theme, then include a file called called content-___.php
		 * (where ___ is the post format) and that will be used instead.
		 */
		get_template_part( 'content', 'page' );
        ?>

        <?php get_template_part( 'page-parts/posts-social-share' ); ?>

        <?php if ( sq_option( 'page_comments', 0 ) == 1 ): ?>

            <!-- Begin Comments -->
            <?php comments_template( '', true ); ?>
            <!-- End Comments -->

        <?php endif; ?>

	<?php endwhile;

endif;
}
?>
  
<div class="goland-section center" style="position: relative;">

	<div class="goland-box title-box">
		 <div class="title">GO connects leaders around the world and invests in their action to inspire sustainable development and progress in their communities.</div>
	</div>

  <!--
	<div class="goland-box news">
		<h1 class="ribbon">
		</h1>
	</div>
  -->

	<div class="goland-box watch-box">
		<?//= do_shortcode('[rev_slider hompage]'); ?>
		<!--<a href="#" id="play-intro-video"><span><i class="icon-play-round-circled"></i></span></a>-->
		<iframe height="100%" width="100%" src="https://www.youtube.com/embed/oadv3tZEMb0?rel=0" frameborder="0" allowfullscreen></iframe>
	</div>

	<div style="width: 100%; position: fixed; bottom: 0px;" class="no-mobile">
		<div class="goland-box scroll-box">
			<div><font size="2.3em">Scroll down to learn more...</font></div>
		</div>
	</div>

</div>

<div class="no-mobile">

<div class="goland-section">
	<div id="infobox1" class="goland-infobox">
		<div class="info-title">Jo & Michelle</div>
		<div><img src='/wp-content/uploads/avatars/17/365a83639de7b0064959369fd77662f7-bpfull.jpg'/></div>
		<div class="content">
			Two best friends from Alaska, Jo and Michelle, decided to put their time and energy towards an important cause and connected with GO.
		</div>
	</div>
</div>

<div class="goland-section">
	<div id="infobox2" class="goland-infobox">
		<div class="info-title">Peter</div>
		<div><img src='/wp-content/uploads/2012/08/Peter2.jpg'/></div>
		<div class="content" style="width: 100%;">
			GO connected them with Peter, a GO leader in Uganda, who had an idea of building a pig farm to make a positive impact in his community.
		</div>
	</div>
</div>

<div class="goland-section">
	<div id="infobox3" class="goland-infobox">
		<div class="info-title">Support</div>
		<div><img src='/wp-content/themes/go-kleo/assets/ico/Logo_168.png'/></div>
		<div class="content">
			The GO Champions set a fundraising goal of $10,000, hosted an event, reached out to family and friends and ultimately achieved their goal to support Peter’s farm. 
		</div>
	</div>
</div>

<div class="goland-section">
	<div id="infobox4" class="goland-infobox">
		<div class="info-title">Project Farm</div>
		<div><img src='/wp-content/uploads/2012/11/Giving-the-first-pig1.jpg' width=300/></div>
		<div class="content" style="width: 100%;">
			The pig farm now provides jobs and food in his community, which GO Champions Jo and Michelle saw with their own eyes when they visited Uganda and continued their relationship with Peter.
		</div>
	</div>
</div>

<div class="goland-section">
	<div id="actionbox">
		<div class="goland-box actionbox">
			<a href="/focusareas/">GO See Projects</a>
		</div>
    <!--
		<div class="goland-box actionbox">
			<a href="/profiles/">GO Meet Champions</a>
		</div>
    -->
	</div>
</div>

</div>

<div class="goland-section"></div>

<?php get_template_part('page-parts/general-after-wrap'); ?>

<?php get_footer(); ?>

<script type="text/javascript">
jQuery(document).ready(function() {
	var titlepos = jQuery('.title-box').position();
	var faded = false;

	var b1 = jQuery('#infobox1');
	var pos1 = b1.position();
	var b2 = jQuery('#infobox2');
	var pos2 = b2.position();
	var b3 = jQuery('#infobox3');
	var pos3 = b3.position();
	var b4 = jQuery('#infobox4');
	var pos4 = b4.position();
	var ab = jQuery('#actionbox');
	var abpos = ab.position();
	jQuery(window).scroll(function() {
		var winpos = jQuery(window).scrollTop();

		if(winpos >= titlepos.top) {
			jQuery('.scroll-box')[0].style.opacity = 0;
		} else {
			jQuery('.scroll-box')[0].style.opacity = 1;		
		}

		if(((pos1.top - winpos) / window.innerHeight) <= 0.05) {
			b1.addClass("stick1");
		} else {
			b1.removeClass("stick1");
		}
		if(((pos2.top - winpos) / window.innerHeight) <= 0.20) {
			b2.addClass("stick2");
		} else {
			b2.removeClass("stick2");
		}
		if(((pos3.top - winpos) / window.innerHeight) <= 0.30) {
			b3.addClass("stick3");
		} else {
			b3.removeClass("stick3");
		}
		if(((pos4.top - winpos) / window.innerHeight) <= 0.55) {
			b4.addClass("stick4");
		} else {
			b4.removeClass("stick4");
		}

		if(((abpos.top - winpos) / window.innerHeight) <= 0.25) {
			ab.addClass("stickab");
		} else {
			ab.removeClass("stickab");
		}

		if(((abpos.top - winpos) / window.innerHeight) <= 0.80 && !faded) {
			b1.fadeTo('slow', 0);
			b2.fadeTo('slow', 0);
			b3.fadeTo('slow', 0);
			b4.fadeTo('slow', 0);
			faded = true;
		} else if(((abpos.top - winpos) / window.innerHeight) > 0.80 && faded) {
			b1.fadeTo('slow', 1);
			b2.fadeTo('slow', 1);
			b3.fadeTo('slow', 1);
			b4.fadeTo('slow', 1);
			faded = false;
		}
	});

	/* click to play video icon */
	jQuery('#play-intro-video').click(function() {
		jQuery('.watch-box').addClass('watch-box-play');
	});


});

</script>

