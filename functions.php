<?php
/*
 go-kleo theme
 functions.php 
*/

add_action( 'wp_enqueue_scripts', 'go_kleo_enqueue_styles' );

function go_kleo_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

// functional code to modify theme after it's gone through it's initial seutp
function gokleo_after_setup_theme() {
	// Abuse the translation mechanisms to replace verbage on the site
	$domains = array('default','kleo_framework','tgmpa','kleo_framework','tgmpa','kleo_framework','tgmpa','tgmpa','kleo_framework','tgmpa','kleo_framework','buddypress','kleo_framework','kleo_framework','yit','kleo_framework','yit','pmpro','credit card type {ending in} xxxx','woocommerce','pmpro','kleo_framework','redux-framework','kleo_framework','default','woocommerce','kleo_framework','woocommerce','woocommerce','kleo_framework','kleo_framework','pmpro','default','kleo_framework','buddypress');
	$path = get_stylesheet_directory();
	foreach ($domains as $domain) {
		load_child_theme_textdomain($domain, $path.'/languages');
	}

	// Remove the show more/less functionality from profiles and groups
	remove_action('bp_before_member_header','kleo_bp_expand_profile', 20 );
	remove_action('bp_before_group_header','kleo_bp_expand_profile', 20 );

	// Remove the add friend button after header, we'll add it ourselves
	remove_action( 'bp_member_header_actions',    'bp_add_friend_button', 5 );

	// Remove the "free" and other badges from "products"
	remove_action( 'woocommerce_before_shop_loop_item_title', 'kleo_woo_loop_badges', 10 );

}
add_action( 'after_setup_theme', 'gokleo_after_setup_theme', 100);

function gokleo_wp_footer() {
	?>
	<script type="text/javascript">

		// Menu Nav links
		mixpanel.track_links('.nav a', 'menu-click', {});

		// Become a champion button
		mixpanel.track_links('.become-champion .button', 'become-champion-click', {});    

		// Proceed to checkout
		mixpanel.track_links('.checkout-button', 'proceed-to-checkout-click', {'amount':jQuery(".order-total .amount")[0].innerHTML});    

		// Donate Button
		jQuery(".single_add_to_cart_button").click(function() {
			// The #input_1_1 seems fragile, would be nice to call this out more specifically
			mixpanel.track('donate-click', {'amount': jQuery(".ginput_amount")[0].value});    
		});

		// Cart Remove Item
		jQuery(".product-remove a.remove").click(function() {
			mixpanel.track('cart-remove', {
				'item-title': jQuery(this).parent().siblings('.product-name').children('a')[0].text,
				'item-amount': jQuery(this).parent().siblings('.product-price').children('.amount')[0].innerHTML
			});    			
		});

	</script>
	<?
}
add_action('wp_footer', 'gokleo_wp_footer', 1000);

function gokleo_get_share_buttons() {
	ob_start();
	?>
	<div class="gokleo_share_buttons">
		<section class="container-wrap social-share-wrap">
			<div class="container">
				<div class="share-links" style="padding: 0px;">
		      
		            <span class="kleo-facebook">
		                <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="post_share_facebook"
		                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=220,width=600');return false;">
		                    <i class="icon-facebook"></i>
		                </a>
		            </span>
		            <span class="kleo-twitter">
		                <a href="https://twitter.com/share?url=<?php the_permalink(); ?>" class="post_share_twitter"
		                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=260,width=600');return false;">
		                    <i class="icon-twitter"></i>
		                </a>
		            </span>
		            <span class="kleo-googleplus">
		                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
		                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
		                    <i class="icon-gplus"></i>
		                </a>
		            </span>
		            <span class="kleo-pinterest">
		                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php if (function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>&description=<?php echo strip_tags(get_the_title()); ?>">
		                    <i class="icon-pinterest-circled"></i>
		                </a>
		            </span>
		            <span class="kleo-mail">
		                <a href="mailto:?subject=<?php echo strip_tags(get_the_title()); ?>&body=<?php the_permalink(); ?>" class="post_share_email">
		                    <i class="icon-mail"></i>
		                </a>
		            </span>

					
		        </div>
			</div>
		</section>
	</div>
	<?
	return ob_get_clean();
}


?>
